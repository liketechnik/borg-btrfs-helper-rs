# SPDX-FileCopyrightText: 2023 Florian Warzecha <liketechnik@disroot.org>
#
# SPDX-License-Identifier: CC0-1.0

{
  mkShell,
  backup-borg-btrfs-rs,
  fenixRustToolchain,
  bashInteractive,
  rust-analyzer,
  reuse,
}:

mkShell {
  nativeBuildInputs = backup-borg-btrfs-rs.nativeBuildInputs ++ [
    fenixRustToolchain

    bashInteractive
    rust-analyzer
    reuse
  ];

  buildInputs = backup-borg-btrfs-rs.buildInputs ++ [
  ];
}

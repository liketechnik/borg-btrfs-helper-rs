// SPDX-FileCopyrightText: 2022 Florian Warzecha <liketechnik@disroot.org>
//
// SPDX-License-Identifier: EUPL-1.2

use std::borrow::Cow::{self, Borrowed, Owned};

use anyhow::Result;
use rustyline::{highlight::Highlighter, Editor, ColorMode, config::Configurer};
use rustyline_derive::{Completer, Helper, Hinter, Validator};

#[derive(Completer, Helper, Hinter, Validator)]
struct PasswordHighlighter {
}

impl Highlighter for PasswordHighlighter {
    fn highlight<'l>(&self, line: &'l str, _pos: usize) -> Cow<'l, str> {
        use unicode_width::UnicodeWidthStr;
        Owned(".".repeat(line.width()))
    }

    fn highlight_char(&self, _line: &str, _pos: usize) -> bool {
        true
    }
}

/// Note: this is not secure (in the way that in merely hides the user input,
/// but does no attempt at securing the input in the memory or preventing key logging
pub fn read_secret(prompt: &str) -> Result<String> {
    let h = PasswordHighlighter { };

    let mut rl = Editor::new()?;
    rl.set_helper(Some(h));

    // force apply the highlighter,
    // thus making sure the password is not visible
    rl.set_color_mode(ColorMode::Forced); 
    // make sure password is not added to history
    rl.set_auto_add_history(false); 

    let secret = rl.readline(prompt)?;
    Ok(secret)
}

// SPDX-FileCopyrightText: 2022 Florian Warzecha <liketechnik@disroot.org>
//
// SPDX-License-Identifier: EUPL-1.2

use std::{path::PathBuf, time::Duration, process::{Command, Stdio, exit}, thread, io::Read, sync::Mutex};


use anyhow::{Result, Context, anyhow};
use clap::Parser;
use dbus::{blocking::Connection, arg::{Variant, PropMap}};
use flexi_logger::{Logger, FileSpec, detailed_format, LogSpecification, Duplicate, LevelFilter, AdaptiveFormat};
use human_panic::setup_panic;
use log::{debug, info, error, warn};
use nix::{unistd::{self, Pid}, libc::{SIGINT, SIGTERM, SIGQUIT, syscall, SYS_ioprio_set, sched_setscheduler, SCHED_RR, sched_param}, sys::{signal::{self, Signal}, wait::wait}, sched::{sched_setaffinity, CpuSet}};
use once_cell::sync::Lazy;
use signal_hook::iterator::Signals;

use crate::secret_input::read_secret;

mod secret_input;

static CHILD_PROCS: Lazy<Mutex<Vec<Pid>>> = Lazy::new(|| {
    Mutex::new(Vec::new())
});

#[derive(Parser)]
#[clap(author, version, about, long_about = None)]
struct Cli {
    /// block device name containing the borg repository to use
    ///
    /// this is expected to be luks encrypted, while the contained
    /// filesystem should be mountable without passing any options
    ///
    /// Note: Do *not* use the full path (i. e. /dev/sda1) to
    /// the device, but only its name (i. e. sda1).
    backup_device: String,
    /// repository location inside the backup block's filesystem
    repository_relative: String,
    /// path where the root subvolume of the btrfs filesystem to 
    /// take backups from is mounted
    btrfs_root: PathBuf,
    /// the subvolumes to backup
    subvolumes: Vec<String>,
    /// the prefix of the backup's name
    ///
    /// default is hostname, yielding backup names
    /// in the form '$hostname-%Y-%m-%d'
    #[clap(short, long)]
    archive_prefix: Option<String>,
    /// only print what would be done, do not execute anything
    #[clap(short = 'n', long)]
    dry_run: bool,
    /// power off backup drive after backups finished successfully
    #[clap(short, long)]
    power_off_backup_drive: bool,
}

// https://stackoverflow.com/a/72862682/16132154
fn execute_and_log(mut command: Command, command_name: &'static str, after_spawn: impl FnOnce(Pid)) -> Result<()> {
    let stdout = Stdio::piped();
    let stderr = Stdio::piped();

    command.stdin(Stdio::null());
    command.stdout(stdout);
    command.stderr(stderr);

    // take the lock for CHILD_PROCS *before* spawning the child
    // process, to make sure we can put its pid there.
    // this happens in a separate scope, so that the mutex lock
    // is dropped after the child has been spawned and before its process
    // exited, to make the mutex accessible (e. g. in the signal handler) while the child process
    // runs
    let mut child;
    let pid;
    {
        let mut procs = CHILD_PROCS.lock().unwrap();
        child = command.spawn()?;
        pid = Pid::from_raw(child.id() as i32);
        procs.push(pid);
    }

    fn handle_stdout(mut stream: impl Read, command_name: &'static str) -> Result<()> {
        let mut buf =  [0u8; 1024];

        loop {
            let num_read = stream.read(&mut buf)?;
            if num_read == 0 {
                break;
            }

            let buf = &buf[..num_read];
            String::from_utf8_lossy(buf).lines().for_each(|l| info!("[{}] {}", command_name, l));
        }

        Ok(())
    }
    fn handle_stderr(mut stream: impl Read, command_name: &'static str) -> Result<()> {
        let mut buf =  [0u8; 1024];

        loop {
            let num_read = stream.read(&mut buf)?;
            if num_read == 0 {
                break;
            }

            let buf = &buf[..num_read];
            String::from_utf8_lossy(buf).lines().for_each(|l| warn!("[{}] {}", command_name, l));
        }

        Ok(())
    }

    let child_stdout = std::mem::take(&mut child.stdout).context("cannot attach to child stdout")?;
    let child_stderr = std::mem::take(&mut child.stderr).context("cannot attach to child stderr")?;

    let thread_stdout = thread::spawn(move || {
        handle_stdout(child_stdout, command_name)
    });
    let thread_stderr = thread::spawn(move || {
        handle_stderr(child_stderr, command_name)
    });

    after_spawn(pid);

    thread_stdout.join().expect("child stdout handler thread failed")?;
    thread_stderr.join().expect("child stderr handler thread failed")?;

    let status = child.wait()?;

    CHILD_PROCS.lock().unwrap().retain(|p| p != &pid);

    if status.success() {
        Ok(())
    } else {
        Err(anyhow!("Command {} failed with exit status {}", command_name, status.code().map_or_else(||"".to_string(), |c| c.to_string())))
    }
}

fn run() -> Result<()> {
    // extract command line arguments
    let cli = Cli::parse();

    if cli.subvolumes.is_empty() {
        return Err(anyhow!("No subvolumes specified. "));
    }
    
    debug!("Connecting to systemd dbus");
    // connect to system dbus
    let conn = Connection::new_system().context("Failed to connnect to system d-bus")?;

    // get passphrases
    let borg_passphrase = read_secret("Borg repository password: ").context("Failed to read password from stdin")?;
    let disk_passphrase = read_secret("Backup disk-encryption password: ").context("Failed to read password from stdin")?;

    // use user supplied archive prefix or get system hostname
    let mut hostname_buffer = [0u8; 64];
    let archive_prefix = cli.archive_prefix.as_ref().map_or_else(|| gethostname(&mut hostname_buffer), |prefix| Ok(prefix))?;

    // init proxy to locked backup device
    let locked_device_proxy = conn.with_proxy("org.freedesktop.UDisks2", format!("/org/freedesktop/UDisks2/block_devices/{}", cli.backup_device), Duration::from_millis(5000));

    info!("Unlocking & mounting backup device");
    // get drive name for later use
    // let (backup_drive_path,): (dbus::strings::Path,) = locked_device_proxy.get("org.freedesktop.UDisks2.Block", "Drive").context("Failed to access backup device's drive name")?; // not working for some reason, throws a dbus error
    let (backup_drive_path,): (Variant<dbus::strings::Path>,) = locked_device_proxy.method_call("org.freedesktop.DBus.Properties", "Get", ("org.freedesktop.UDisks2.Block", "Drive")).context("Failed to access backup device's drive name")?;

    // unlock the device
    let (unlocked_object,): (dbus::strings::Path,) = locked_device_proxy.method_call("org.freedesktop.UDisks2.Encrypted", "Unlock", (disk_passphrase, PropMap::new())).context("Failed to unlock backup device")?;

    // get a proxy to the unlocked device and mount it
    let unlocked_device_proxy = conn.with_proxy("org.freedesktop.UDisks2", unlocked_object, Duration::from_millis(5000));
    let (backup_root_path,): (String,) = unlocked_device_proxy.method_call("org.freedesktop.UDisks2.Filesystem", "Mount", (PropMap::new(),)).context("Failed to mount unlocked backup device")?;

    let repo_path = format!("{}/{}", backup_root_path, cli.repository_relative);

    for subvolume in cli.subvolumes {
        let snapshot_name = format!("{}_backup", subvolume);
        let archive_name = format!("{}-{}", archive_prefix, subvolume.replace('/', "_"));

        let mut subvolume_path = PathBuf::new();
        subvolume_path.push(&cli.btrfs_root);
        subvolume_path.push(&snapshot_name);

        if subvolume_path.exists() {
            warn!("snapshot {} of subvolume {} already exists, skipping creation", snapshot_name, subvolume);
        } else {
            let mut subvol_creation = Command::new("btrfs");
            subvol_creation
                .arg("subvolume")
                .arg("snapshot")
                .arg("-r")
                .arg(&subvolume)
                .arg(&snapshot_name);
            subvol_creation.current_dir(&cli.btrfs_root);
            execute_and_log(subvol_creation, "btrfs", |_| {}).context("failed to create snapshot of subvolume")?;
        }

        let mut backup = Command::new("borg");
        backup
            .arg("create")
            .arg("--verbose")
            .arg("--stats")
            .arg("--show-rc")
            .arg("--compression").arg("zstd,19")
            .arg("--noatime")
            .arg("--exclude").arg("dev")
            .arg("--exclude").arg("proc")
            .arg("--exclude").arg("tmp")
            .arg("--exclude").arg("run")
            .arg("--exclude").arg("root/.cache")
            .arg("--exclude").arg(".cache")
            .arg("--exclude").arg("var/tmp")
            .arg("--exclude").arg("var/run")
            .arg("--exclude").arg("var/lock")
            .arg("--exclude").arg("media")
            .arg("--exclude").arg("mnt")
            .arg("--exclude").arg("home/florian/.cache")
            .arg("--exclude").arg("florian/.cache")
            .arg(format!("{}::{}-{{now:%Y-%m-%d}}", repo_path, archive_name))
            .arg(".");
        backup.current_dir(subvolume_path);
        backup.env("BORG_PASSPHRASE", &borg_passphrase);
        execute_and_log(backup, "borg", |pid| {
                #[allow(unused_must_use)]
                {
                    let mut cpuset = CpuSet::new();
                    cpuset.set(3);
                    sched_setaffinity(pid, &cpuset);
                }

                unsafe {
                    const IOPRIO_WHO_PROCESS: u32 = 1;
                    const IOPRIO_CLASS_RT: u32 = 1;
                    const IOPRIO_CLASS_SHIFT: u32 = 13;

                    // let prio: u32 = ioprio | ioprio_class << IOPRIO_CLASS_SHIFT
                    #[allow(clippy::identity_op)]
                    let prio: u32 = 0 | IOPRIO_CLASS_RT << IOPRIO_CLASS_SHIFT;

                    syscall(SYS_ioprio_set, IOPRIO_WHO_PROCESS, pid.as_raw(), prio);

                    let param = sched_param { sched_priority: 99, };
                    sched_setscheduler(pid.as_raw(), SCHED_RR, &param);
                }
            }).context("failed to create backup")?;

        let mut subvol_creation = Command::new("btrfs");
        subvol_creation
            .arg("subvolume")
            .arg("delete")
            .arg(&snapshot_name);
        subvol_creation.current_dir(&cli.btrfs_root);
        execute_and_log(subvol_creation, "btrfs", |_| {}).context("failed to delete snapshot of subvolume")?;
    }

    info!("Unmount & locking device");
    // unmount and lock the backup device
    unlocked_device_proxy.method_call("org.freedesktop.UDisks2.Filesystem", "Unmount", (PropMap::new(),)).context("Failed to unmount backup device")?;
    locked_device_proxy.method_call("org.freedesktop.UDisks2.Encrypted", "Lock", (PropMap::new(),)).context("Failed to lock backup device")?;

    if cli.power_off_backup_drive {
        info!("Powering off backup device");
        // turn off the backup drive
        let backup_drive_proxy = conn.with_proxy("org.freedesktop.UDisks2", backup_drive_path.0, Duration::from_millis(5000));
        backup_drive_proxy.method_call("org.freedesktop.UDisks2.Drive", "PowerOff", (PropMap::new(),)).context("Failed to power off backup device's drive")?;
    }

    Ok(())
}

fn gethostname(buffer: &mut [u8]) -> Result<&str> {
    let hostname_cstr = unistd::gethostname(buffer).context("Failed getting hostname")?;
    let hostname = hostname_cstr.to_str().context("Hostname is not valid UTF-8")?;
    Ok(hostname)
}

fn main() {
    setup_panic!();

    let logspec_env = LogSpecification::env_or_parse("debug").unwrap();
    let logspec = LogSpecification::builder()
        .insert_modules_from(logspec_env)
        .module("rustyline", LevelFilter::Warn)
        .build();

    let _logger = Logger::with(logspec)
        .format_for_files(detailed_format)
        .adaptive_format_for_stderr(AdaptiveFormat::Opt)
        .log_to_file(
            FileSpec::default()
                .directory("logs")
        )
        .duplicate_to_stderr(Duplicate::All)
        .print_message()
        .set_palette("1;3;2;4;6".to_string())
        .start().unwrap();

    let mut signals = Signals::new(&[SIGINT, SIGTERM, SIGQUIT]).expect("Signal handlers");

    thread::spawn(move || {
        for sig in signals.forever() {
            error!("Received signal {}, exiting", sig);

            let child_procs = CHILD_PROCS.lock().unwrap();

            if !child_procs.is_empty() {
                warn!("{} child-processes currently running. Waiting for them to finish...", child_procs.len());
            }

            for child in child_procs.iter() {
                if let Err(e) = signal::kill(*child, Some(Signal::try_from(sig).unwrap_or(Signal::SIGTERM))) {
                    warn!("Failed to pass signal to pid {}: {:?}", child, e);
                } else {
                    debug!("Passed signal to pid {}", child)
                }
            }

            while wait().is_ok() {
            }

            exit(1);
        }
    });

    match run() {
        Ok(_) => (),
        Err(e) => error!("{:#}", e),
    }
}

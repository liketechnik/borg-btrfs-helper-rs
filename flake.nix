# SPDX-FileCopyrightText: 2023 Florian Warzecha <liketechnik@disroot.org>
#
# SPDX-License-Identifier: CC0-1.0

{
  description = "flake for wrapper script around borg and btrfs";

  inputs.nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";

  inputs.flake-utils.url = "github:numtide/flake-utils";

  inputs.fenix = {
    url = "github:nix-community/fenix";
    inputs.nixpkgs.follows = "nixpkgs";
  };

  inputs.nix-filter = {
    url = "github:numtide/nix-filter";
  };

  outputs = { self, nixpkgs, flake-utils, fenix, ... }@inputs:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs {
          inherit system;
        };

        fenix-pkgs = fenix.packages.${system};
        fenix-channel = fenix-pkgs.complete;
      in rec {
        packages.default = pkgs.callPackage ./nix/packages/backup-borg-btrfs-rs.nix {
          nix-filter = import inputs.nix-filter;
          rustPlatform = pkgs.makeRustPlatform {
            cargo = fenix-channel.toolchain;
            rustc = fenix-channel.toolchain;
          };
        };
        devShells.default = pkgs.callPackage ./nix/dev-shells/backup-borg-btrfs-rs.nix {
          backup-borg-btrfs-rs = packages.default;
          fenixRustToolchain = fenix-channel.withComponents [
            "cargo"
            "clippy-preview"
            "rust-src"
            "rustc"
            "rustfmt-preview"
          ];
          rust-analyzer = fenix-pkgs.rust-analyzer;
        };
      }
    );
}
